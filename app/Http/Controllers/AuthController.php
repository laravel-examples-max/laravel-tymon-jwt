<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginRequest;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Tymon\JWTAuth\Exceptions\JWTException;
use Symfony\Component\HttpFoundation\Response;

class AuthController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['login']]);
    }

    /**
     * @OA\Post(
     *     path="/api/auth/login",
     *     summary="Generar Token con Inició de Sesión",
     *     tags={"Sesions"},
     *     @OA\Response(
     *         response=200,
     *         description="Obtener informacion de token."
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="Ha ocurrido un error."
     *     ),
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\JsonContent(ref="#/components/schemas/LoginRequest")
     *     ),
     * )
     */
    public function login(LoginRequest $loginRequest)
    {

        $credentials = $loginRequest->only('email', 'password');

        //Crean token
        try {
            if (!$token = auth()->attempt($credentials)) {
                return response()->json([
                    'success' => false,
                    'message' => 'Credenciales invalidas, intente de nuevo.',
                ], 401);
            }
        } catch (JWTException $e) {
            return $credentials;
            return response()->json([
                'success' => false,
                'message' => 'No se pudo crear token.',
            ], 500);
        }

        //Token created, return with success response and jwt token
        return $this->respondWithToken($token , 60);
    }

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me()
    {
        $user = \auth()->user();

        return response()->json([
            'data' => [
                'id' => $user->id,
                'name' => $user->nama,
                'email' => $user->email,
            ],
        ]);
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        try {

            $cookie = \Cookie::forget('jwt');
            auth()->logout();

            return response()->json([
                'success' => true,
                'message' => 'Sesión finalizada.'
            ])->withCookie($cookie);

        } catch (JWTException $exception) {
            return response()->json([
                'success' => false,
                'message' => 'Lo sentimos, no se finalizo la sesión.'
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken(\auth()->refresh() , 20160);
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token , $timeToken)
    {
        $cookie = cookie('jwt' , $token , $timeToken * 24);

        return response()->json([
            'success' => true,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * $timeToken,
            'access_token' => $token,
        ])->withCookie($cookie);
    }


}
