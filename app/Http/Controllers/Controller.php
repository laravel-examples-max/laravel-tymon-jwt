<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * @OA\Info(
     *     version="1.0.0",
     *     title="API JWT con Tymon",
     *     description="JWT validacion costumisadas para fines de prueba y practica con Tymon",
     *     @OA\Contact(email="codemax120@gmail.com"),
     *     @OA\License(name="Apache 2.0", url="http://www.apache.org/licenses/LICENSE-2.0.html")
     * )
     *
     * @OA\Server(
     *     url=L5_SWAGGER_CONST_HOST,
     *     description="JWT Local Server",
     * )
     *
     *
     * @OA\SecurityScheme(
     *     securityScheme="bearerAuth",
     *     type="http",
     *     scheme="bearer",
     * )
     *
     * @OA\Tag(
     *     name="Tymon JWT",
     *     description="API Endpoints of JWT"
     * )
     */
}
