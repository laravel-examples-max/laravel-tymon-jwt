<?php

namespace App\Console\Commands;

use App\Models\User;
use Illuminate\Console\Command;
use Symfony\Component\Console\Output\ConsoleOutput;

class TokenCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'token:generate {id}';

    public function handle()
    {
        $id = $this->argument('id');
        $user = User::find($id);
        $email = $user->email;
        $credentials = ["email" => $email, "password" => 'password'];
        $console = new ConsoleOutput();

        if($token = auth()->attempt($credentials)) {
            $console->writeln($token);
        } else {
            $console->writeln('No se genero Token, porque no se encontro el id de User');
        }

    }
}
